package com.example.emailsender;

import java.util.List;

public abstract class EmailService {

	public abstract void sendEmail(String mailSendFrom, String sendTo, String sendToCC,String mailSubject, String messageBody);
	public abstract void sendEmail(String mailSendFrom, List<String> sendTo, List<String> sendToCC, String mailSubject, String messageBody);

}
