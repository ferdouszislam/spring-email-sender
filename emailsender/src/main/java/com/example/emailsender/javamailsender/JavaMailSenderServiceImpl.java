package com.example.emailsender.javamailsender;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.example.emailsender.EmailService;

@Service
public class JavaMailSenderServiceImpl extends EmailService{
	Logger Log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public void sendEmail(String mailSendFrom, String sendTo, String sendToCC, String mailSubject, String messageBody) {
		Log.info("<<<<<----- sendEmail service is called. ----->>>>>");
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(mailSendFrom);
        message.setTo(sendTo);
        message.setCc(sendToCC);
        message.setSubject(mailSubject);
        message.setText(messageBody);

        mailSender.send(message);
	}

	@Override
	public void sendEmail(String mailSendFrom, List<String> sendTo, List<String> sendToCC, String mailSubject, String messageBody) {
		try {
			Log.info("<<<<<----- sendEmail service is called. ----->>>>>");
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setFrom(mailSendFrom);
			helper.setSubject(mailSubject);
			sendTo.forEach(recipient -> {
				try {
					helper.addTo(new InternetAddress(recipient));
				} catch (AddressException e) {
					Log.error("<<<<<----- Exception {} has occured in sendEmail service is called. ----->>>>>", e.getMessage());
					e.printStackTrace();
				} catch (MessagingException e) {
					Log.error("<<<<<----- Exception {} has occured in sendEmail service is called. ----->>>>>", e.getMessage());
					e.printStackTrace();
				}
			});
			sendToCC.forEach(ccRecipient -> {
				try {
					helper.addCc(new InternetAddress(ccRecipient));
				} catch (AddressException e) {
					Log.error("<<<<<----- Exception {} has occured in sendEmail service is called. ----->>>>>", e.getMessage());
					e.printStackTrace();
				} catch (MessagingException e) {
					Log.error("<<<<<----- Exception {} has occured in sendEmail service is called. ----->>>>>", e.getMessage());
					e.printStackTrace();
				}
			});
			helper.setText(messageBody, true);
			mailSender.send(message);
			System.out.println("Email sent successfully!");
		} catch (MessagingException e) {
			Log.error("<<<<<----- Exception {} has occured in sendEmail service is called. ----->>>>>", e.getMessage());
			e.printStackTrace();
		}		
	}

}
