package com.example.emailsender.javamailsender;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class JavaMailSenderConfig {

	@Value("${spring.mail.username}")
	private String username;

	@Value("${spring.mail.password}")
	private String password;

	@Value("${spring.mail.host}")
	private String mailSenderHost;

	@Value("${spring.mail.port}")
	private int mailSenderPort;

	@Value("${spring.mail.properties.mail.transport.protocol}")
	private String transportProtocol;

	@Value("${spring.mail.properties.mail.smtp.auth}")
	private String smtpAuth;

	@Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	private String startTlsEnable;
	
	@Value("${spring.mail.properties.mail.smtp.starttls.required}")
    private String startTlsRequired;

	@Value("${spring.mail.debug}")
	private String mailDebug;

	@Bean
	public JavaMailSender getJavaMailSender() {

		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailSenderHost);
		mailSender.setPort(mailSenderPort);
		mailSender.setUsername(username);
		mailSender.setPassword(password);
		Properties properties = mailSender.getJavaMailProperties();
		properties.put("mail.transport.protocol", transportProtocol);
	    properties.put("mail.smtp.auth", smtpAuth);
	    properties.put("mail.smtp.starttls.enable", startTlsEnable);
	    properties.put("mail.smtp.starttls.required", startTlsRequired);
	    properties.put("mail.debug", mailDebug);

		return mailSender;
	}
	
}
