package com.example.emailsender;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceHelper {

//	@Value("${com.nsl.document.smartSearchWebURL}")
//	private String smartSearchWebURL;

	@Value("${spring.mail.username}")
	private String senderUsername;

	@Autowired
	@Qualifier("awsSesServiceImpl")
	private EmailService awsSesService;
	
	@Autowired
	@Qualifier("javaMailSenderServiceImpl")
	private EmailService javaMailSenderService;

	public void triggerEmailWithAwsSes(String uploader, List<String> recipients, List<String> documentTitles) {
		if(!recipients.isEmpty()) {
			String mailSubject = this.getEmailSubject(uploader, documentTitles);
			String mailBody = this.getEmailBody(uploader, documentTitles, null);
			awsSesService.sendEmail(senderUsername, recipients, new ArrayList<>(), mailSubject, mailBody);
		}
	}
	
	public void triggerEmailWithJavaMail(String uploader, List<String> recipients, List<String> documentTitles) {
		if(!recipients.isEmpty()) {
			String mailSubject = this.getEmailSubject(uploader, documentTitles);
			String mailBody = this.getEmailBody(uploader, documentTitles, null);
			javaMailSenderService.sendEmail(senderUsername, recipients, new ArrayList<>(), mailSubject, mailBody);
		}
	}

	private String getEmailSubject(String uploader, List<String> documentTitles) {
		String emailSubject = uploader + " shared \"";
		int totalTitles = documentTitles.size();
		if(Objects.equals(totalTitles, 0)) {
			return emailSubject+"\" with you";
		}
		else if(Objects.equals(totalTitles, 1)) {
			return emailSubject + documentTitles.get(0) +"\" with you.";
		} else {
			return emailSubject + documentTitles.get(0) +"\" and "+(totalTitles-1)+" more document(s) with you.";
		}
	}

	private String getEmailBody(String uploader, List<String> documentTitles, String messagesFromUploader) {
		String mailBody = "<p><i>"+uploader+"</i> shared document(s) with you. Document(s) titles are: </p>";
					for(int i=0;i<documentTitles.size();i++) {
						mailBody += "<p>"+(i+1) +". " +documentTitles.get(i)+"</p>";
					}
					mailBody += "<p>Search for the documents that <i>"+uploader+"</i> shared with you by clicking the following link.</p>";
//					+ "<p><a href="+smartSearchWebURL+"> Smart Search</a></p>";
		if(Objects.nonNull(messagesFromUploader) && !messagesFromUploader.isEmpty()) {
			mailBody = mailBody + "<p>Note : "+messagesFromUploader+"</p>";
		}
		return mailBody;
	}
}
