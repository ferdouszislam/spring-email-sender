package com.example.emailsender.awsses;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.example.emailsender.EmailService;

@Service
public class AwsSesServiceImpl extends EmailService {

	private Logger Log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MailSender mailSender;
	
	@Value("${spring.mail.username}")
	private String emailFrom;
	
	@Override
	public void sendEmail(String mailSendFrom, String sendTo, String sendToCC,
			String mailSubject, String messageBody) {
		try {
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(emailFrom);
			simpleMailMessage.setTo(sendTo);
			simpleMailMessage.setCc(sendToCC);
			simpleMailMessage.setSubject(mailSubject);
			simpleMailMessage.setText(messageBody);
			mailSender.send(simpleMailMessage);
		} catch (Exception e) {
			Log.info("exception: {} occurred in {}", e, "sendEmail");
			e.printStackTrace();
		}
	}

	@Override
	public void sendEmail(String mailSendFrom, List<String> sendTo,
			List<String> sendToCC, String mailSubject, String messageBody) {
		try {
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setFrom(emailFrom);
			simpleMailMessage.setTo(sendTo.toArray(new String[sendTo.size()]));
			simpleMailMessage.setCc(sendToCC.toArray(new String[sendToCC.size()]));
			simpleMailMessage.setSubject(mailSubject);
			simpleMailMessage.setText(messageBody);
			mailSender.send(simpleMailMessage);
		} catch (Exception e) {
			Log.info("exception: {} occurred in {}", e, "sendEmail");
			e.printStackTrace();
		}
	}

}
