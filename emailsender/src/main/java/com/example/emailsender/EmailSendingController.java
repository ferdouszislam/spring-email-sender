package com.example.emailsender;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailSendingController {

	private Logger Log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EmailServiceHelper emailServiceHelper;
	
	private List<String> DEFAULT_TO_LIST = Arrays.asList(
			"sbappy@gfoundries.com",
			"mohammadhemayet.ullah@globalfoundries.com",
			"mehedi.hasan@neural-semiconductor.com",
			"ferdous.islam@neural-semiconductor.com",
			"mohammad.ashikur@neural-semiconductor.com");
	
	@RequestMapping(value = "/send-email", method = RequestMethod.GET)
	public ResponseEntity<?> sendEmail(
			@RequestParam(name = "to", required = false) List<String> to,
			@RequestParam(name = "emailServiceToUse", required = false) String emailServiceToUse) {
		try {
			Log.info("sendEmail called with to: {}", to);
			if (Objects.isNull(to) || to.isEmpty()) {
				to = DEFAULT_TO_LIST;
			}
			if (Objects.equals(emailServiceToUse, "java-ms")) {
				emailServiceHelper.triggerEmailWithJavaMail(
						"java-ms-email-sender-service", to, Arrays.asList("document1.docx", "document2.docx"));
			} else {
				emailServiceHelper.triggerEmailWithAwsSes("aws-ses-email-sender-service", to, Arrays.asList("document1.docx", "document2.docx"));
			}
			return ResponseEntity.ok("email sent!");
		} catch (Exception e) {
			Log.info("exception: {} occurred.", e.getMessage());
			e.printStackTrace();
			return ResponseEntity.internalServerError().body("exception: " + e.getMessage() + " occurred.");
		}
	}
	
}
