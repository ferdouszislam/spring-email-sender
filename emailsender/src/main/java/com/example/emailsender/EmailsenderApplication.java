package com.example.emailsender;

import java.io.IOException;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class EmailsenderApplication {

	public static long appStartTime;
	
	public static void main(String[] args) throws IOException {
		SpringApplication.run(EmailsenderApplication.class, args);
		appStartTime = System.currentTimeMillis();
	}

	@GetMapping("/")
	public String index() {
		return "email sender service running since : " + new Date(EmailsenderApplication.appStartTime);
	}

}
